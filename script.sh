#!/bin/bash
/home/administrador/Documentos/Platanus/platanus assemble -m 10 -k 15 -o k15/k15 -f reads_tarea4/lib1_*.fq
/home/administrador/Documentos/Platanus/platanus assemble -m 10 -k 20 -o k20/k20 -f reads_tarea4/lib1_*.fq
/home/administrador/Documentos/Platanus/platanus assemble -m 10 -k 30 -o k30/k30 -f reads_tarea4/lib1_*.fq
/home/administrador/Documentos/Platanus/platanus assemble -m 10 -k 40 -o k40/k40 -f reads_tarea4/lib1_*.fq
/home/administrador/Documentos/Platanus/platanus assemble -m 10 -k 50 -o k50/k50 -f reads_tarea4/lib1_*.fq
/home/administrador/Documentos/Platanus/platanus assemble -m 10 -k 60 -o k60/k60 -f reads_tarea4/lib1_*.fq
/home/administrador/Documentos/Platanus/platanus assemble -m 10 -k 70 -o k70/k70 -f reads_tarea4/lib1_*.fq
/home/administrador/Documentos/Platanus/platanus assemble -m 10 -k 80 -o k80/k80 -f reads_tarea4/lib1_*.fq
/home/administrador/Documentos/Platanus/platanus assemble -m 10 -k 85 -o k85/k85 -f reads_tarea4/lib1_*.fq
/home/administrador/Documentos/Platanus/platanus scaffold -OP reads_tarea4/lib2_1.fq reads_tarea4/lib2_2.fq -c k15/k15_contig.fa -o k15/scaff_k15
/home/administrador/Documentos/Platanus/platanus scaffold -OP reads_tarea4/lib2_1.fq reads_tarea4/lib2_2.fq -c k20/k20_contig.fa -o k20/scaff_k20
/home/administrador/Documentos/Platanus/platanus scaffold -OP reads_tarea4/lib2_1.fq reads_tarea4/lib2_2.fq -c k30/k30_contig.fa -o k30/scaff_k30
/home/administrador/Documentos/Platanus/platanus scaffold -OP reads_tarea4/lib2_1.fq reads_tarea4/lib2_2.fq -c k40/k40_contig.fa -o k40/scaff_k40
/home/administrador/Documentos/Platanus/platanus scaffold -OP reads_tarea4/lib2_1.fq reads_tarea4/lib2_2.fq -c k50/k50_contig.fa -o k50/scaff_k50
/home/administrador/Documentos/Platanus/platanus scaffold -OP reads_tarea4/lib2_1.fq reads_tarea4/lib2_2.fq -c k60/k60_contig.fa -o k60/scaff_k60
/home/administrador/Documentos/Platanus/platanus scaffold -OP reads_tarea4/lib2_1.fq reads_tarea4/lib2_2.fq -c k70/k70_contig.fa -o k70/scaff_k70
/home/administrador/Documentos/Platanus/platanus scaffold -OP reads_tarea4/lib2_1.fq reads_tarea4/lib2_2.fq -c k80/k80_contig.fa -o k80/scaff_k80
/home/administrador/Documentos/Platanus/platanus scaffold -OP reads_tarea4/lib2_1.fq reads_tarea4/lib2_2.fq -c k85/k85_contig.fa -o k85/scaff_k85
python3 /home/administrador/Documentos/Platanus/quast/quast.py k15/scaff_k10_scaffold.fa -o quast_platanus/ -m 0
python3 /home/administrador/Documentos/Platanus/quast/quast.py k20/scaff_k20_scaffold.fa -o quast_platanus/ -m 0
python3 /home/administrador/Documentos/Platanus/quast/quast.py k30/scaff_k30_scaffold.fa -o quast_platanus/ -m 0
python3 /home/administrador/Documentos/Platanus/quast/quast.py k40/scaff_k40_scaffold.fa -o quast_platanus/ -m 0
python3 /home/administrador/Documentos/Platanus/quast/quast.py k50/scaff_k50_scaffold.fa -o quast_platanus/ -m 0
python3 /home/administrador/Documentos/Platanus/quast/quast.py k60/scaff_k60_scaffold.fa -o quast_platanus/ -m 0
python3 /home/administrador/Documentos/Platanus/quast/quast.py k70/scaff_k70_scaffold.fa -o quast_platanus/ -m 0
python3 /home/administrador/Documentos/Platanus/quast/quast.py k80/scaff_k80_scaffold.fa -o quast_platanus/ -m 0
python3 /home/administrador/Documentos/Platanus/quast/quast.py k85/scaff_k85_scaffold.fa -o quast_platanus/ -m 0
