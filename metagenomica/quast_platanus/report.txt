All statistics are based on contigs of size >= 6 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    scaff_k15_scaffold  scaff_k25_scaffold  scaff_k35_scaffold  scaff_k45_scaffold  scaff_k55_scaffold  scaff_k65_scaffold  scaff_k75_scaffold
# contigs (>= 0 bp)         137057              3567                3408                5420                9613                3                   3                 
# contigs (>= 1000 bp)      0                   305                 304                 354                 5                   0                   0                 
# contigs (>= 5000 bp)      0                   52                  67                  43                  0                   0                   0                 
# contigs (>= 10000 bp)     0                   16                  32                  15                  0                   0                   0                 
# contigs (>= 25000 bp)     0                   8                   11                  2                   0                   0                   0                 
# contigs (>= 50000 bp)     0                   1                   6                   0                   0                   0                   0                 
Total length (>= 0 bp)      3502044             2155802             2541666             2617676             1967533             383                 369               
Total length (>= 1000 bp)   0                   1081470             1547210             904434              5752                0                   0                 
Total length (>= 5000 bp)   0                   702518              1195618             429213              0                   0                   0                 
Total length (>= 10000 bp)  0                   453667              948349              227268              0                   0                   0                 
Total length (>= 25000 bp)  0                   325516              599216              53354               0                   0                   0                 
Total length (>= 50000 bp)  0                   64932               422737              0                   0                   0                   0                 
# contigs                   137057              3567                3408                5420                9613                3                   3                 
Largest contig              528                 64932               130491              26984               1413                148                 133               
Total length                3502044             2155802             2541666             2617676             1967533             383                 369               
GC (%)                      34.10               36.15               36.03               36.01               36.04               35.77               42.01             
N50                         25                  1005                2717                714                 224                 130                 131               
N90                         15                  211                 242                 194                 120                 105                 105               
auN                         41.1                9165.4              19636.4             2753.8              261.5               130.1               124.3             
L50                         37986               302                 86                  842                 2983                2                   2                 
L90                         113711              2011                1692                3527                7814                3                   3                 
# N's per 100 kbp           0.00                8270.61             11194.00            5507.67             8.84                0.00                0.00              
